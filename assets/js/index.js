'use strict';

const GOVUK = require('govuk-frontend');
window.GOVUK = GOVUK;

const $ = require('jquery');
window.jQuery = $;

const sessionDialog = require('./session-dialog');

window.GOVUK.initAll();
GOVUK.sessionDialog.init();
