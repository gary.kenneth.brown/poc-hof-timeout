# Overview
Proof of concept to demonstrate adding the accessible timeout feature into a HOF application.

# Running

Ensure all Node dependencies are in place:

```
npm install
```

Start up a redis instance. Docker command:

```
docker start redis
```

Run the application:

```
npm run start:dev
```

Note: There are issues with hot reloading when you make changes to any code. Terminate the running process using `Ctrl+C` and restart using the above command.

Open a browser and go to: http://localhost:8080/hof1 Wait for 30 seconds for the dialog to be displayed.

# Impact

* Add the client side resources `session-dialog.js` and `session-dialog.scss` which were taken from the existing Java IHS Claim application and add the files to `/assets/js` and `/assets/scss` respectively. These will be compiled by `hof-build` into `bundle.js` used by the server when running.

* There is code in the `/assets/js/index.js` to reference the new library and initialise it when loaded in the browser.

```
const sessionDialog = require('./session-dialog');
GOVUK.sessionDialog.init();
```

* Add in jQuery 3+ as a dependency in the Node modules and include in `/assets/js/index.js` as shown below:

```
const $ = require('jquery');
window.jQuery = $;
```

Note: Assiging jQuery `$` to `window.jQuery` allows the session-dialog code to reference jQuery as `$` only has function scope with index.js. Adding it to a global object such as `window` allows other modules to reference it. Same thing with the *GOVUK* object.

* Create a *view* `step.html` to override the existing HOF step template with the additional HTML for all form pages. The HTML will include the necessary `<dialog>` for the accessible timeout alert.

* Add the `<dialog>` HTML to `views/step.html` as shown below:

```
...

    {{#input-submit}}continue{{/input-submit}}
    <dialog id="js-modal-dialog"
			data-session-timeout="60" 
			data-session-timeout-warning="30" 
			data-session-timeout-final-warning="15" 
			data-url-redirect="/session-timeout"
		  class="modal-dialog dialog" role="dialog" aria-live="polite" aria-labelledby="dialog-title" aria-describedby="at-timer">
		<div class="modal-dialog__inner">
		    <h1 id="dialog-title" class="govuk-heading-l">
		      Your application will time out soon
		    </h1>
		    <div class="modal-dialog__inner__text">
		      <div id="timer" class="timer" aria-hidden="true" aria-relevant="additions"></div>
		      <div id="at-timer" class="at-timer govuk-visually-hidden" role="status"></div>
		    </div>
		    <div class="modal-dialog__inner__block">
		      <button class="govuk-button dialog-button modal-dialog__inner__button js-dialog-close">Continue application</button>
		    </div>
		</div>
	</dialog>
{{/page-content}}
```

* Update the `<dialog>` attributes for the necessary timeout settings. See the table below for a description of each setting.

* For non-javascript users, a meta `<noscript` tag is added to the header of the HTML document. This is added to the same `step.html` earlier in our `apps/{appname}/views`.

```
{{<partials-page}}
	{{$head}}
		{{> partials-head}}
		<noscript>
			<meta http-equiv="refresh" content="60;url='/session-timeout'"/>
		</noscript>
	{{/head}}

...
```

# Dialog settings

| config setting | description | example value |
|----------------|-------------|---------------|
| `data-session-timeout` | Number of seconds when the user session will timeout | 1800 |
| `data-session-timeout-warning` | seconds to display the warning message popup BEFORE session timeout. I.e how many seconds remaining before the session expires. | 300 |
| `data-session-timeout-final-warning` | Same as `data-session-timeout-warning`. Final warning alert shown before the session timeout. | 10 |
| `data-url-redirect` | Url to redirect users to when the session has timed out. Activated after `data-session-timeout` has elapsed with no browser --> server activity | `/session-timeout` |
