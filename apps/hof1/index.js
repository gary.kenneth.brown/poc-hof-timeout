'use strict';

module.exports = {
  name: 'hof1',
  baseUrl: '/hof1',
  steps: {
    '/name': {
      fields: ['given-name', 'family-name'],
      next: '/dob'
    },
    '/dob': {
      fields: ['date-of-birth'],
      next: '/ihs-number'
    },
    '/ihs-number': {
      fields: ['ihs-number'],
      next: '/address'
    },
    '/address': {
      fields: ['address-line-1', 'address-line-2', 'town', 'postcode'],
      next: '/confirm'
      },
    '/confirm': {
      //behaviours: require('hof').components.summary,
      next: '/complete'
    },
    '/complete': {
      template: 'confirmation'
    }
  }
};
